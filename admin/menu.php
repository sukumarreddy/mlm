<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="<?php if ($page == "Dashboard") echo "active"; ?>"><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
            <li class="treeview <?php if ($page == "Members") echo "active"; ?>">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Members</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php if ($page1 == "Add Member") echo "class='active'"; ?>><a href="member.php"><i class="fa fa-user-plus"></i> Add Member</a></li>
                        <li <?php if ($page1 == "Members") echo "class='active'"; ?>><a href="members.php"><i class="fa fa-users"></i>View Members</a></li>
                    </ul>
            </li>
            <li class="<?php if ($page == "genealogy") echo "active"; ?>"><a href="tree.php" ><i class="fa fa-tree"></i><span>Genealogy</span></a></li>
            <li><a><i class="fa fa-shopping-basket"></i> <span>Orders</span></a></li>
			
                 <li class="treeview <?php if ($page == "Payment") echo "active"; ?>">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Payment</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php if ($page1 == "View Today Hold") echo "class='active'"; ?>><a href="#"><i class="fa fa-circle-o"></i> Today Hold</a></li>
                        <li <?php if ($page1 == "Add Customer") echo "class='active'"; ?>><a href="#"><i class="fa fa-circle-o"></i> Today Pending</a></li>
                    </ul>

                </li>

            <li><a href="logout.php"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
            </li>
        </ul>
    </section>
</aside>