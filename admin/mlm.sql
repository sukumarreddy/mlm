-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2020 at 07:40 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mlm`
--

-- --------------------------------------------------------

--
-- Table structure for table `mlmpassword`
--

DROP TABLE `mlmpassword`;
CREATE TABLE `mlmpassword` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `rID` int(11) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `md` varchar(64) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) NOT NULL,
  `photo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mlmpassword`
--

INSERT INTO `mlmpassword` (`id`, `pid`, `rID`, `password`, `name`, `email`, `md`, `ip`, `datetime`, `status`, `photo`) VALUES
(1, 0, 0, 'admin', 'admin', 'admin', '47ac-8165-046e-f153', '::1', '2020-02-08 05:21:36', 1, '1.jpg'),
(3, 1, 1, 'v23', 'kannan', 'kannan@v23.in', '4000-9746-a9ed-9b96', '::1', '2020-02-08 05:36:20', 1, '1.jpg'),
(4, 1, 1, 'v23', 'sukumar', 'sukumar@v23.in', '45bd-8a60-61d0-d47d', '::1', '2020-02-08 05:37:41', 1, '1.jpg'),
(5, 1, 1, 'admin', 'lal', 'lal@v23.in', '4d7b-b430-d6f2-d689', '::1', '2020-02-08 05:38:19', 1, '1.jpg'),
(6, 1, 1, 'v23', 'vikas', 'vikas@v23.in', '49e1-a604-f300-5082', '::1', '2020-02-08 05:40:51', 1, '1.jpg'),
(7, 3, 3, 'v23', 'k1', 'k1@v23.in', '4e1b-a441-d48e-4960', '::1', '2020-02-08 05:43:26', 1, '1.jpg'),
(8, 3, 3, 'v23', 'k2', 'k2@v23.in', '466b-9bcc-b12b-de16', '::1', '2020-02-08 05:43:53', 1, '1.jpg'),
(9, 7, 7, 'v23', 'k3', 'k3@v23.in', '4a61-9297-02e5-d49e', '::1', '2020-02-08 05:44:19', 1, '1.jpg'),
(13, 1, 1, 'samsu', 'Samsu', 'samsu@gmail.com', '42c0-820b-1553-3818', '::1', '2020-02-11 02:09:42', 1, '13.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mlmregistration`
--
DROP TABLE `mlmregistration`;
CREATE TABLE `mlmregistration` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `father_name` varchar(50) DEFAULT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `marital_status` varchar(20) DEFAULT NULL,
  `dob` varchar(10) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `aadhar_no` varchar(20) DEFAULT NULL,
  `pan` varchar(20) DEFAULT NULL,
  `payee_name` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `account_no` varchar(20) DEFAULT NULL,
  `branch` varchar(50) DEFAULT NULL,
  `ifsc_code` varchar(20) DEFAULT NULL,
  `nominee_name` varchar(50) DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  `nominee_age` varchar(3) DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `country` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `pincode` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `md` varchar(64) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mlmregistration`
--

INSERT INTO `mlmregistration` (`id`, `name`, `father_name`, `mother_name`, `gender`, `marital_status`, `dob`, `occupation`, `aadhar_no`, `pan`, `payee_name`, `bank_name`, `account_no`, `branch`, `ifsc_code`, `nominee_name`, `relation`, `nominee_age`, `address`, `phone`, `mobile`, `country`, `state`, `city`, `pincode`, `email`, `status`, `md`, `username`, `pid`) VALUES
(1, 'Administrator', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'admin', 0, '47ac-8165-046e-f153', '', 1),
(2, 'Kannan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'kannan@v23.in', 0, '4000-9746-a9ed-9b96', '', 3),
(3, 'sukumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'sukumar@v23.in', 0, '45bd-8a60-61d0-d47d', '', 4),
(4, 'lal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'lal@v23.in', 0, '4d7b-b430-d6f2-d689', '', 5),
(5, 'vikas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'vikas@v23.in', 0, '49e1-a604-f300-5082', '', 6),
(6, 'jerry', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'k1@v23.in', 0, '4e1b-a441-d48e-4960', '', 7),
(7, 'micky', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'k2@v23.in', 0, '466b-9bcc-b12b-de16', '', 8),
(8, 'kiran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 'k3@v23.in', 0, '4a61-9297-02e5-d49e', '', 9),
(12, 'Samsu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'address', '', '', 'India', '', '', '', 'samsu@gmail.com', 0, '42c0-820b-1553-3818', '', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mlmpassword`
--
ALTER TABLE `mlmpassword`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mlmregistration`
--
ALTER TABLE `mlmregistration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mlmpassword`
--
ALTER TABLE `mlmpassword`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mlmregistration`
--
ALTER TABLE `mlmregistration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
