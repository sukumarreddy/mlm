$('.Number').keypress(function (event) {
    var charCode = event.which;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
});

$('.AlphaNumeric').keypress(function (event) {
    var charCode = event.which;
    var charStr = String.fromCharCode(charCode);
    if(/[a-z0-9]/i.test(charStr)){
        return true;
    }
    return false;
});

$('.Decimal').keypress(function(evt) {  
    var $txtBox = $(this);
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
        return false;
    else {
        var len = $txtBox.val().length;
        var index = $txtBox.val().indexOf('.');
        if (index > 0 && charCode == 46) {
          return false;
        }
        if (index > 0) {
            var charAfterdot = (len + 1) - index;
            if (charAfterdot > 3) {
                return false;
            }
        }
    }
    return $txtBox; 
});