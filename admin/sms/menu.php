<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">

               <li class="<?php if($page=="Dashboard") echo "active"; ?>"><a href="admin.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
			   <ul class="treeview  <?php if($page1=="Dashboard1") echo "active"; ?>">
			     </ul>
</li>
    
	<?php if(($_SESSION['user_type']=="Admin") || ($_SESSION['user_type']=="Staff")) { ?>
          <li class="treeview <?php if($page=="SMS Details") echo "active"; ?>">
          <a href="#">
            <i class="fa fa-user-plus"></i>
            <span>SMS Details</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li <?php if($page1=="Auto Send SMS") echo "class='active'"; ?>><a href="auto-send.php"><i class="fa fa-circle-o"></i> Auto Send SMS</a></li>
             <li <?php if($page1=="SMS Sending List") echo "class='active'"; ?>><a href="sending-sms.php"><i class="fa fa-circle-o"></i> SMS Sending List</a></li>
          </ul>
        </li>
		<li class="treeview <?php if($page=="Customers") echo "active"; ?>">
          <a href="#">
            <i class="fa fa-user-plus"></i>
            <span>Customers</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

             <li <?php if($page1=="View Customers") echo "class='active'"; ?>><a href="customers.php"><i class="fa fa-circle-o"></i> View Customers</a></li>
             <li <?php if($page1=="Add Customer") echo "class='active'"; ?>><a href="add-customer.php"><i class="fa fa-circle-o"></i> Add Customer </a></li>
          </ul>
        </li>
		<li class="treeview <?php if($page=="Users") echo "active"; ?>">
          <a href="#">
            <i class="fa fa-user-plus"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
             <li <?php if($page1=="Add User") echo "class='active'"; ?>><a href="add-user.php"><i class="fa fa-circle-o"></i> Add User </a></li>
             <li <?php if($page1=="View Users") echo "class='active'"; ?>><a href="view-users.php"><i class="fa fa-circle-o"></i> View Users</a></li>
          </ul>
        </li>
		<li class="treeview <?php if($page=="Profile") echo "active"; ?>">
          <a href="#">
            <i class="fa fa-user-plus"></i>
            <span>Profile</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($page1=="My Profile") echo "class='active'"; ?>><a href="my-profile.php"><i class="fa fa-circle-o"></i> My Profile</a></li>
             <li <?php if($page1=="Change Password") echo "class='active'"; ?>><a href="password.php"><i class="fa fa-circle-o"></i> Change Password </a></li>
             <li <?php if($page1=="Logout") echo "class='active'"; ?>><a href="logout.php"><i class="fa fa-circle-o"></i> Logout</a></li>

          </ul>
        </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>