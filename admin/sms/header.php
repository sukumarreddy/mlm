<?php
$date=date("Y-m-d");
$time=date('h:iA');

?>
<header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>M</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Customer</b>&nbsp;Management</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
	  
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
		   <br/>
			</li>



          <?php

                            $sql5 = "select * from ssc_users";
                        $result5 = mysqli_query($conn, $sql5);
                        while ($row5 = mysqli_fetch_assoc($result5)) {
                            ?>   
			 
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">
              <?php echo $row5['full_name']; ?>&nbsp;<div class="caret"></div>
                </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              
              <li class="user-header">
              <img src="photo/<?php echo $row5['photo']; ?>?<?php echo rand(); ?>"/>

                <p>
                <?php echo $row5['full_name']; ?>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
    <li class="user-footer">
 <?php if(($_SESSION['user_type']=="admin")) { ?>
              <b><a class="btn btn-default btn-flat" href="edit-admin-record.php?id=<?php echo $row5['id']; ?>">Profile</a></b>
 <?php } ?>
  <?php if(($_SESSION['user_type']=="principal")) { ?>

              <b><a class="btn btn-default btn-flat" href="edit-principal-record.php?id=<?php echo $row5['id']; ?>">Profile</a></b>
 <?php } ?>
  <?php if(($_SESSION['user_type']=="staff")) { ?>

              <b><a class="btn btn-default btn-flat" href="edit-staff-record.php?id=<?php echo $row5['id']; ?>">Profile</a></b>
 <?php } ?>
 <?php if(($_SESSION['user_type']=="student")) { ?>

              <b><a class="btn btn-default btn-flat" href="view-student-record.php?id=<?php echo $row5['id']; ?>">Profile</a></b>
 <?php } ?>
             <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Logout</a>
                </div>
              </li>
			               <?php } ?>

            </ul>
          </li>
          
          <!-- Control Sidebar Toggle Button -->
         <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
  </header>