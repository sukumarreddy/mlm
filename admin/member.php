<?php
session_start();
include "config.php";
include "functions.php";
include "timeout.php";
$page = "add_member";
$page1 = "Add Member";
$id = 0;
$pid = 0;
$mode = "add";
$msg = "";
$color = "red";

$photo="";
$name="";
$email="";
$password = "";
$father_name="";
$mother_name="";
$gender="";
$address="";
$mobile="";
$phone="";
$marital_status="";
$dob="";
$occupation="";
$state="";
$pincode="";
$city="";
$aadhar_no="";
$pan="";
$payee_name="";
$bank_name="";
$account_no="";
$branch="";
$ifsc_code="";
$nominee_name="";
$relation="";
$nominee_age="";

$id = isset($_GET['id']) ? $_GET['id'] : 0 ;
$md = isset($_GET['md']) ? $_GET['md'] : 0 ;
$mode = isset($_GET['mode']) ? $_GET['mode'] : "add" ;

if($mode == "edit"){
    $sql = "select a.*,b.password,b.photo from mlmregistration a,mlmpassword b where a.pid=b.id and b.md = '$md'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);
    $photo = $row['photo'];
    $id = $row['id'];
    $pid = $row['pid'];
    $name=$row['name'];
    $email=$row['email'];
    $password = $row['password'];
    $father_name=$row['father_name'];
    $mother_name=$row['mother_name'];
    $gender=$row['gender'];
    $address=$row['address'];
    $mobile=$row['mobile'];
    $phone=$row['phone'];
    $marital_status=$row['marital_status'];
    $dob=$row['dob'];
    $occupation=$row['occupation'];
    $state=$row['state'];
    $pincode=$row['pincode'];
    $city=$row['city'];
    $aadhar_no=$row['aadhar_no'];
    $pan=$row['pan'];
    $payee_name=$row['payee_name'];
    $bank_name=$row['bank_name'];
    $account_no=$row['account_no'];
    $branch=$row['branch'];
    $ifsc_code=$row['ifsc_code'];
    $nominee_name=$row['nominee_name'];
    $relation=$row['relation'];
    $nominee_age=$row['nominee_age'];
}

if(isset($_POST['submit'])){
    $email=$_POST['email'];
    if(duplicate_email($email,$pid)){
        $msg = "Email already exists";
    }else{
        $id = $_SESSION['id'];
        $rID = $_SESSION['rID'];
        $parent_id = $_SESSION['pid'];
        $parent_id = 0;
        $reference_id = 0;
        if($id == 1){
            $reference_id = 1;
        }else{
            $reference_id = $id;
        }
        $parent_id = get_parent_id($id);
        $name=$_POST['name'];
        $father_name=$_POST['father_name'];
        $mother_name=$_POST['mother_name'];
        $gender=$_POST['gender'];
        $marital_status=$_POST['marital_status'];
        $dob=$_POST['dob'];
        $occupation=$_POST['occupation'];
        $aadhar_no=$_POST['aadhar_no'];
        $pan=$_POST['pan'];
        $payee_name=$_POST['payee_name'];
        $bank_name=$_POST['bank_name'];
        $account_no=$_POST['account_no'];
        $branch=$_POST['branch'];
        $ifsc_code=$_POST['ifsc_code'];
        $nominee_name=$_POST['nominee_name'];
        $relation=$_POST['relation'];
        $nominee_age=$_POST['nominee_age'];
        $address=$_POST['address'];
        $phone=$_POST['phone'];
        $mobile=$_POST['mobile'];
        $country='India';
        $state=$_POST['state'];
        $city=$_POST['city'];
        $pincode=$_POST['pincode'];
        $md = uid($email);
        $username='';
        $password = $_POST['password'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $datetime = date("Y-m-d H:i:s");
        if($mode=="add"){
            $sql = "insert into mlmpassword (pid,rID,password,name,email,md,ip,datetime,status) values ('$parent_id','$reference_id','$password','$name','$email','$md','$ip','$datetime',1)";
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
            $pid = mysqli_insert_id($conn);
            $sql = "insert into mlmregistration (name,father_name,mother_name,gender,marital_status,dob,occupation,aadhar_no,pan,payee_name,bank_name,account_no,branch,ifsc_code,nominee_name,relation,nominee_age,address,mobile,phone,country,state,city,pincode,email,status,md,username,pid) values ('$name','$father_name','$mother_name','$gender','$marital_status','$dob','$occupation','$aadhar_no','$pan','$payee_name','$bank_name','$account_no','$branch','$ifsc_code','$nominee_name','$relation','$nominee_age','address','$mobile','$phone','$country','$state','$city','$pincode','$email',0,'$md','','$pid')";
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
            $id = mysqli_insert_id($conn);
        }
        if(trim($_FILES["photo"]["name"])!=""){
            $target_dir = "lib/template/photo/";
            $extension = strtolower(pathinfo($_FILES["photo"]["name"],PATHINFO_EXTENSION));
            $photo = $pid . "." .$extension;
            $target_file = $target_dir . $photo;
            if(file_exists($target_file)) unlink($target_file);
            $uploadedfile = $_FILES['photo']['tmp_name']; 
            $src = imagecreatefromjpeg($uploadedfile);        
            list($width, $height) = getimagesize($uploadedfile); 
            $tmp = imagecreatetruecolor(100, 100); 
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, 100, 100, $width, $height); 
            imagejpeg($tmp, $target_file, 100);
            $sql = "update  mlmpassword set photo='$photo' where id=$pid";
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
        }
        if($mode=="edit"){
            $sql = "update  mlmpassword set name='$name',email='$email' where id=$pid";
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
            $sql = "update mlmregistration set name='$name',father_name='$father_name',mother_name='$mother_name',gender='$gender',marital_status='$marital_status',dob='$dob',occupation='$occupation',aadhar_no='$aadhar_no',pan='$pan',payee_name='$payee_name',bank_name='$bank_name',account_no='$account_no',branch='$branch',ifsc_code='$ifsc_code',nominee_name='$nominee_name',relation='$relation',nominee_age='$nominee_age',address='$address',mobile='$mobile',phone='$phone',state='$state',city='$city',pincode='$pincode',email='$email' where id='$id'";
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
        }
        if(trim($mobile!="" && $mode=="add")){
            $message = "Welcome to Eyarkai Enterprises";
            send_sms($mobile,$message);
        }
        header("location: members.php");
    } 
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Member</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include "header.php"; ?>
    <?php include "menu.php"; ?>
    <div class="content-wrapper">
        <section class="content">
            <form name="myform" method="post" action="" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading" align="center">
                                <?php if($mode == "edit"){; ?>
                                    <h1 class="panel-title">Edit Member</h1>
                                <?php }else{ ?>
                                    <h1 class="panel-title">Add Member</h1>
                                <?php } ?>
                                <?php if($msg!=""){ ?>
                                <div class="alert alert-danger" role="alert">
                                    <?php echo $msg; ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading panel-info" align="center">
                                <h1 class="panel-title">Login Information</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label class="required">Name</label>
                                        <input value="<?php echo $name; ?>" autofocus required maxlength="50" name="name" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label class="required">Email</label>
                                        <input value="<?php echo $email; ?>" required maxlength="50" id="email" name="email" class="form-control" type="email"/>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label class="required">Password</label>
                                        <input value="<?php echo $password; ?>" required maxlength="10" id="password" name="password" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label class="required">Confirm Password</label>
                                        <input value="<?php echo $password; ?>" required maxlength="10" id="confirm_password" name="confirm_password" class="form-control" type="text"/>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Photo</label>
                                        <input accept="image/jpeg" type="file" name="photo" id="photo" class="form-control" />
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <?php if($photo!=""){ ?>
                                            <img src="lib/template/photo/<?php echo $photo; ?>" width="60" height="60" />
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-success">
                            <div class="panel-heading panel-success" align="center">
                                <h1 class="panel-title">Personal Details</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Father Name</label>
                                        <input value="<?php echo $father_name; ?>" maxlength="50" name="father_name" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>Mother Name</label>
                                        <input value="<?php echo $mother_name; ?>" maxlength="50" name="mother_name" class="form-control" type="text"/>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Gender</label>
                                        <select name="gender" class="form-control"> 
                                            <option value="">Select One</option>
                                            <option <?php if($gender=="Male") echo " selected "; ?> value="Male">Male</option>
                                            <option <?php if($gender=="Female") echo " selected "; ?> value="Female">Female</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>Address</label>
                                        <textarea maxlength="500" name="address" class="form-control" ><?php echo $address; ?></textarea>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Mobile</label>
                                        <input value="<?php echo $mobile; ?>" maxlength="15" name="mobile" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>Alternate Phone</label>
                                        <input value="<?php echo $phone; ?>" maxlength="15" name="phone" class="form-control" type="text"/>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Marital Status</label>
                                        <select name="marital_status" class="form-control"> 
                                            <option value="">Select One</option>
                                            <option <?php if($marital_status=="Single") echo " selected "; ?> value="Single">Single</option>
                                            <option <?php if($marital_status=="Married") echo " selected "; ?> value="Married">Married</option>
                                            <option <?php if($marital_status=="Divorced") echo " selected "; ?> value="Divorced">Divorced</option>
                                            <option <?php if($marital_status=="Widowed") echo " selected "; ?> value="Widowed">Widowed</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>Date of Birth</label>
                                        <input name="dob" class="form-control" type="date"/>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Occupation</label>
                                        <input value="<?php echo $occupation; ?>" maxlength="50" name="occupation" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>State</label>
                                        <select name="state" class="form-control"> 
                                            <option value="">Select One</option>
                                            <option <?php if($state=="Tamil Nadu") echo " selected "; ?> value="Tamil Nadu">Tamil Nadu</option>
                                            <option <?php if($state=="Kerala") echo " selected "; ?> value="Kerala">Kerala</option>
                                        </select>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Pincode</label>
                                        <input value="<?php echo $pincode; ?>" maxlength="15" name="pincode" class="form-control Number" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>City</label>
                                        <input value="<?php echo $city; ?>" maxlength="50" name="city" class="form-control" type="text"/>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Aadhar No</label>
                                        <input value="<?php echo $aadhar_no; ?>" maxlength="20" name="aadhar_no" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>PAN</label>
                                        <input value="<?php echo $pan; ?>" maxlength="20" name="pan" class="form-control" type="text"/>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading panel-success" align="center">
                                <h1 class="panel-title">Bank Details</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6 form-group">
                                        <label>Payee Name</label>
                                        <input value="<?php echo $payee_name; ?>" maxlength="50" name="payee_name" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <label>Bank Name</label>
                                        <input value="<?php echo $bank_name; ?>" maxlength="50" name="bank_name" class="form-control" type="text"/>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 form-group">
                                        <label>Account Number</label>
                                        <input value="<?php echo $account_no; ?>" maxlength="20" name="account_no" class="form-control Number" type="text"/>
                                    </div>
                                    <div class="col-xs-4 form-group">
                                        <label>Branch</label>
                                        <input value="<?php echo $branch; ?>" maxlength="50" name="branch" class="form-control" type="text"/>
                                    </div>     
                                    <div class="col-xs-4 form-group">
                                        <label>IFSC Code</label>
                                        <input value="<?php echo $ifsc_code; ?>" maxlength="20" name="ifsc_code" class="form-control" type="text"/>
                                    </div>                           
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 form-group">
                                        <label>Nominee Name</label>
                                        <input value="<?php echo $nominee_name; ?>" maxlength="50" name="nominee_name" class="form-control" type="text"/>
                                    </div>                                
                                    <div class="col-xs-4 form-group">
                                        <label>Relation with Nominee</label>
                                        <input value="<?php echo $relation; ?>" maxlength="50" name="relation" class="form-control" type="text"/>
                                    </div>
                                    <div class="col-xs-4 form-group">
                                        <label>Nominee Age</label>
                                        <input value="<?php echo $nominee_age; ?>" maxlength="3" name="nominee_age" class="form-control Number" type="text"/>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group text-center">
                                <button style="display: none;" name="submit" type="submit" id="submit_button">Not Shown</button>
                                <input id="normal_button" class="btn btn-info" type="button" name="sub" value="Save"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <?php include "footer.php"; ?>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/demo.js"></script>
<script src="js/helper.js"></script>
<script>
var id = "<?php echo $id; ?>";

$('#normal_button').click(function(e){
    var password = $('#password').val().trim();
    var confirm_password = $('#confirm_password').val().trim();
    if(password != confirm_password){
        alert("Password mismatch");
        $('#confirm_password').focus();
        return;
    }    
    var email = $('#email').val().trim();
    $.ajax({
        type: "POST",
        url: "check_email.php",
        data: {email:email,id:id},
        success: function(response) {
            console.log(response);
            if(response == "duplicate"){
                alert("Email already exists");
                $('#email').focus();
                return;
            }else{
                $("#submit_button").click();
            }
        }
    });
 });

</script>
</body>
</html>